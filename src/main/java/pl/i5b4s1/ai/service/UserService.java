package pl.i5b4s1.ai.service;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.form.ChangePasswordForm;
import pl.i5b4s1.ai.enums.RoleEnum;
import pl.i5b4s1.ai.exception.IncorrectOldPassword;
import pl.i5b4s1.ai.model.Doctor;
import pl.i5b4s1.ai.model.Patient;
import pl.i5b4s1.ai.model.User;
import pl.i5b4s1.ai.repository.DoctorRepository;
import pl.i5b4s1.ai.repository.PatientRepository;
import pl.i5b4s1.ai.repository.UserRepository;

import javax.persistence.EntityNotFoundException;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, PatientRepository patientRepository, DoctorRepository doctorRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void changePassword(ChangePasswordForm changePasswordForm) {
        User user = userRepository.findById(changePasswordForm.getId()).orElseThrow(EntityNotFoundException::new);
        if (!bCryptPasswordEncoder.matches(changePasswordForm.getOldPassword(),user.getPassword())) {
            throw new IncorrectOldPassword(user.getUsername());
        }
        user.setPassword(bCryptPasswordEncoder.encode(changePasswordForm.getNewPassword()));
        userRepository.save(user);
    }

    public Integer findIdByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username)).getId();
    }

    public String findFullName(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        if (user.getRole().getRole().equals(RoleEnum.ROLE_PATIENT.toString())) {
            Patient patient = patientRepository.findById(user.getId()).orElseThrow(EntityNotFoundException::new);
            return patient.getFirstName() + " " + patient.getLastName();
        } else if (user.getRole().getRole().equals(RoleEnum.ROLE_DOCTOR.toString())) {
            Doctor doctor = doctorRepository.findById(user.getId()).orElseThrow(EntityNotFoundException::new);
            return doctor.getFirstName() + " " + doctor.getLastName();
        }
        return "*";
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                AuthorityUtils.createAuthorityList(user.getRole().getRole())
        );
    }
}
