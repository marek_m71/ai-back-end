package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.form.OpinionForm;
import pl.i5b4s1.ai.model.Opinion;
import pl.i5b4s1.ai.model.Visit;
import pl.i5b4s1.ai.repository.OpinionRepository;
import pl.i5b4s1.ai.repository.VisitRepository;

import javax.persistence.EntityExistsException;

@Service
public class OpinionService {
    private final OpinionRepository opinionRepository;
    private final VisitRepository visitRepository;

    public OpinionService(OpinionRepository opinionRepository, VisitRepository visitRepository) {
        this.opinionRepository = opinionRepository;
        this.visitRepository = visitRepository;
    }

    public void add(OpinionForm opinionForm) {
        Visit visit = visitRepository.findById(opinionForm.getIdVisit()).orElseThrow(EntityExistsException::new);
        opinionRepository.save(convertToOpinion(visit, opinionForm));
    }

    private Opinion convertToOpinion(Visit visit, OpinionForm in) {
        return Opinion.builder()
                .opinion(in.getOpinion())
                .rate(in.getRate())
                .visit(visit)
                .build();
    }
}
