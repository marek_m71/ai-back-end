package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.SpecializationDTO;
import pl.i5b4s1.ai.dto.form.SpecializationAddForm;
import pl.i5b4s1.ai.model.Doctor;
import pl.i5b4s1.ai.model.Specialization;
import pl.i5b4s1.ai.repository.DoctorRepository;
import pl.i5b4s1.ai.repository.SpecializationRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SpecializationService {
    private final SpecializationRepository specializationRepository;
    private final DoctorRepository doctorRepository;

    public SpecializationService(SpecializationRepository specializationRepository, DoctorRepository doctorRepository) {
        this.specializationRepository = specializationRepository;
        this.doctorRepository = doctorRepository;
    }

    public Specialization findOrCreateIfNotExist(String name){
        return specializationRepository.findByName(name).orElseGet(() -> save(name));
    }

    private Specialization save(String name) {
        return specializationRepository.save(Specialization.builder()
                .name(name)
                .build());
    }

    public List<SpecializationDTO> findAll(){
        return specializationRepository.findAll().stream().map(this::convertToSpecializationDto).collect(Collectors.toList());
    }

    public void addToDoctor(SpecializationAddForm specializationAddForm) {
        Doctor doctor = doctorRepository.findById(specializationAddForm.getIdDoctor()).orElseThrow(EntityNotFoundException::new);
        doctor.getSpecializationList().add(findOrCreateIfNotExist(specializationAddForm.getSpecialization().getName()));
        doctorRepository.save(doctor);
    }

    public void deleteFromDoctor(int idDoctor, int idSpecialization) {
        Doctor doctor = doctorRepository.findById(idDoctor).orElseThrow(EntityNotFoundException::new);
        doctor.getSpecializationList().remove(specializationRepository.findById(idSpecialization).orElseThrow(EntityNotFoundException::new));
        doctorRepository.save(doctor);
    }

    private SpecializationDTO convertToSpecializationDto(Specialization in){
        return SpecializationDTO.builder()
                            .id(in.getId())
                            .name(in.getName())
                            .build();
    }



}
