package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.i5b4s1.ai.dto.MedicalServiceDTO;
import pl.i5b4s1.ai.model.MedicalService;
import pl.i5b4s1.ai.repository.MedicalServiceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicalServiceService {
    private final MedicalServiceRepository medicalServiceRepository;

    public MedicalServiceService(MedicalServiceRepository medicalServiceRepository) {
        this.medicalServiceRepository = medicalServiceRepository;
    }

    public List<MedicalServiceDTO> findAll(){
        return medicalServiceRepository.findAll().stream().map(this::convertToMedicalServiceDto).collect(Collectors.toList());
    }

    private MedicalServiceDTO convertToMedicalServiceDto(MedicalService in){
        return MedicalServiceDTO.builder()
                .name(in.getName())
                .build();
    }

    MedicalService findOrCreateIfNotExists(String name) {
        return medicalServiceRepository.findByName(name).orElseGet(() -> save(name));
    }

    private MedicalService save(String name) {
        return medicalServiceRepository.save(new MedicalService(name));
    }

}
