package pl.i5b4s1.ai.service;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.i5b4s1.ai.dto.VisitDetailsDoctorDTO;
import pl.i5b4s1.ai.email.EmailSender;

import java.time.LocalDateTime;

@Service
public class MessageService {
    private final EmailSender emailSender;
    private final TemplateEngine templateEngine;
    private final Environment environment;

    public MessageService(EmailSender emailSender, TemplateEngine templateEngine, Environment environment) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
        this.environment = environment;
    }

    public void sendEmailCancelVisit(String emailUser, LocalDateTime localDateTime, String fullName, String phoneNumber) {
        Context context = new Context();
        context.setVariable("data", localDateTime);
        context.setVariable("fullName", fullName);
        context.setVariable("phoneNumber", phoneNumber);
        String body = templateEngine.process("emailCancelVisit", context);
        emailSender.sendEmail(environment.getProperty("spring.mail.username"), emailUser, "Odwołano wizytę", body);
    }
}
