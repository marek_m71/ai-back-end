package pl.i5b4s1.ai.security;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class SecurityConstants {
    static final List<String> ALLOWED_ORIGIN_METHODS = Arrays.asList("GET", "POST", "PUT", "DELETE");
    static final String SECRET = SecretGenerator.getSecret();
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final Long SESSION_EXPIRATION_TIME = Long.parseLong("864000000");
}
