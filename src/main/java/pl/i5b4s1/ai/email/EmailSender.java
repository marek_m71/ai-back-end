package pl.i5b4s1.ai.email;

public interface EmailSender {
    void sendEmail(String from, String to, String subject, String content);
}