package pl.i5b4s1.ai.dto;

import lombok.*;

import java.util.List;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDTO {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String city;
    private List<SpecializationDTO> specializationList;
    private List<PriceOfTheServiceDTO> priceList;
    private List<VisitDTO> visitList;
    private List<OpinionDTO> opinionList;
}
