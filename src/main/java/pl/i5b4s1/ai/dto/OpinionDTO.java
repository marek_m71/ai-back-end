package pl.i5b4s1.ai.dto;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OpinionDTO {
    private Integer id;
    private Integer rate;
    private String opinion;
}
