package pl.i5b4s1.ai.dto;

import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitDTO {
    private Integer id;
    private LocalDateTime dateTimeVisit;
    private Boolean isAppointed;
    private Boolean isCanceledByDoctor;
}
