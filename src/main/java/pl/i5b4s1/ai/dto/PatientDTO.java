package pl.i5b4s1.ai.dto;

import lombok.*;

import java.util.List;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PatientDTO {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private List<VisitDetailsDoctorDTO> visitList;
}
