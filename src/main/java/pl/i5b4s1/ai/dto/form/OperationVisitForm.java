package pl.i5b4s1.ai.dto.form;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OperationVisitForm {
    private Integer idVisit;
}
