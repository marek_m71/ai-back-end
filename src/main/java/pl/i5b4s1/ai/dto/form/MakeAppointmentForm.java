package pl.i5b4s1.ai.dto.form;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MakeAppointmentForm {
    private Integer idPatient;
    private Integer idVisit;
}
