package pl.i5b4s1.ai.dto.form;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitAddForm {
    private Integer idDoctor;
    private List<VisitForm> visitList;
}
