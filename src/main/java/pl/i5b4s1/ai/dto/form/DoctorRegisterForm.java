package pl.i5b4s1.ai.dto.form;

import lombok.*;
import pl.i5b4s1.ai.dto.PriceOfTheServiceDTO;
import pl.i5b4s1.ai.dto.SpecializationDTO;

import java.util.List;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DoctorRegisterForm {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String city;
    private List<SpecializationDTO> specializationList;
    private List<PriceOfTheServiceForm> priceList;
    private List<VisitForm> visitList;
}
