package pl.i5b4s1.ai.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class PriceOfTheServiceDTO {
    private Integer id;
    private String name;
    private Double price;
}
