package pl.i5b4s1.ai.dto;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SpecializationDTO {
    private Integer id;
    private String name;
}
