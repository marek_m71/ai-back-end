package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.i5b4s1.ai.dto.VisitDTO;
import pl.i5b4s1.ai.dto.form.OperationVisitForm;
import pl.i5b4s1.ai.dto.form.VisitAddForm;
import pl.i5b4s1.ai.service.VisitService;

@RestController
@RequestMapping("/visits")
public class VisitController {
    private final VisitService visitService;

    public VisitController(VisitService visitService) {
        this.visitService = visitService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody VisitAddForm visitAddForm){
        visitService.addVisit(visitAddForm);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VisitDTO> find(@PathVariable("id") int id) {
        return ResponseEntity.status(HttpStatus.OK).body(visitService.findVisit(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteVisit(@PathVariable("id") int id) {
        visitService.deleteVisit(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/patient-cancel")
    public ResponseEntity patientCancelVisit(@RequestBody OperationVisitForm operationVisitForm) {
        visitService.patientCancelVisit(operationVisitForm);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/doctor-cancel")
    public ResponseEntity doctorCancelVisit(@RequestBody OperationVisitForm operationVisitForm) {
        visitService.doctorCancelVisit(operationVisitForm);
        return new ResponseEntity(HttpStatus.OK);
    }

}
