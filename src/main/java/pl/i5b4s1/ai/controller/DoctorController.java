package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.i5b4s1.ai.dto.*;
import pl.i5b4s1.ai.dto.form.DoctorRegisterForm;
import pl.i5b4s1.ai.dto.form.SearchDoctorsForm;
import pl.i5b4s1.ai.service.DoctorService;

import java.util.List;

@RestController
@RequestMapping("/doctors")
public class DoctorController {
    private final DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody DoctorRegisterForm doctorRegisterForm){
        doctorService.add(doctorRegisterForm);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DoctorDTO> update(@PathVariable("id") int id, @RequestBody DoctorDTO doctorDTO){
        return ResponseEntity.status(HttpStatus.OK).body(doctorService.update(id, doctorDTO));
    }

    @PostMapping("/find")
    public ResponseEntity<List<DoctorDTO>> findSome(@RequestBody SearchDoctorsForm searchDoctorsForm) {
        return new ResponseEntity<>(doctorService.findSome(searchDoctorsForm), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DoctorDTO> find(@PathVariable("id") int id) {
        return ResponseEntity.status(HttpStatus.OK).body(doctorService.find(id));
    }

    @GetMapping("/{id}/details")
    public ResponseEntity<DoctorDetailsDTO> findDetails(@PathVariable("id") int id) {
        return ResponseEntity.status(HttpStatus.OK).body(doctorService.findDetails(id));
    }

}
