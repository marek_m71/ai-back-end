package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.i5b4s1.ai.dto.form.ChangePasswordForm;
import pl.i5b4s1.ai.service.UserService;

@RestController
@RequestMapping("/password")
public class PasswordController {
    private final UserService userService;

    public PasswordController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/change")
    public ResponseEntity change(@RequestBody ChangePasswordForm changePasswordForm){
        userService.changePassword(changePasswordForm);
        return new ResponseEntity(HttpStatus.OK);
    }
}
