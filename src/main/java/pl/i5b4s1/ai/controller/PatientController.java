package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.i5b4s1.ai.dto.PatientDTO;
import pl.i5b4s1.ai.dto.form.MakeAppointmentForm;
import pl.i5b4s1.ai.dto.form.PatientRegisterForm;
import pl.i5b4s1.ai.service.PatientService;

@RestController
@RequestMapping("/patients")
public class PatientController {
    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody PatientRegisterForm patientRegisterForm){
        patientService.addPatient(patientRegisterForm);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PatientDTO> update(@PathVariable("id") int id, @RequestBody PatientDTO patientDTO){
        return ResponseEntity.status(HttpStatus.OK).body(patientService.updatePatient(id, patientDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PatientDTO> find(@PathVariable("id") int id){
        return ResponseEntity.status(HttpStatus.OK).body(patientService.findPatient(id));
    }

    @PostMapping("/make-appointment")
    public ResponseEntity makeAppointment(@RequestBody MakeAppointmentForm makeAppointmentForm){
        patientService.makeAppointment(makeAppointmentForm);
        return new ResponseEntity(HttpStatus.OK);
    }
}
