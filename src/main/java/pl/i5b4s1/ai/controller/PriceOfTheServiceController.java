package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.i5b4s1.ai.dto.form.PriceOfTheServiceAddForm;
import pl.i5b4s1.ai.service.PriceOfTheServiceService;

@RestController
@RequestMapping("/price-of-the-service")
public class PriceOfTheServiceController {
    private final PriceOfTheServiceService priceOfTheServiceService;

    public PriceOfTheServiceController(PriceOfTheServiceService priceOfTheServiceService) {
        this.priceOfTheServiceService = priceOfTheServiceService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody PriceOfTheServiceAddForm priceOfTheServiceAddForm){
        priceOfTheServiceService.add(priceOfTheServiceAddForm);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable("id") int id){
        priceOfTheServiceService.remove(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
