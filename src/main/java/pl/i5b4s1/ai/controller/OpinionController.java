package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.i5b4s1.ai.dto.form.OpinionForm;
import pl.i5b4s1.ai.service.OpinionService;

@RestController
@RequestMapping("/opinions")
public class OpinionController {
    private final OpinionService opinionService;

    public OpinionController(OpinionService opinionService) {
        this.opinionService = opinionService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody OpinionForm opinionForm){
        opinionService.add(opinionForm);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
