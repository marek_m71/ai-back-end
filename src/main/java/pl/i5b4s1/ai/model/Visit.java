package pl.i5b4s1.ai.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "visits")
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_visit")
    private Integer id;
    @Column(name = "date")
    private LocalDateTime dateTime;
    @Column(name = "is_appointed")
    private Boolean isAppointed;
    @Column(name = "is_canceled_by_doctor")
    private Boolean isCanceledByDoctor;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "id_patient")
    private Patient patient;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "id_doctor")
    private Doctor doctor;
    @OneToOne(mappedBy = "visit", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Opinion opinion;
}
