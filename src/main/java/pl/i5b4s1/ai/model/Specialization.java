package pl.i5b4s1.ai.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "specializations")
public class Specialization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_specialization")
    private Integer id;
    @Column(name = "name")
    private String name;
    @ManyToMany(mappedBy = "specializationList")
    private List<Doctor> doctors;

}
