package pl.i5b4s1.ai.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "patients")
public class Patient {
    @Id
    @Column(name = "id_user")
    private Integer id;
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private User user;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
    private List<Visit> visitList = new ArrayList<>();

    public void addVisit(Visit visit) {
        visit.setPatient(this);
        this.visitList.add(visit);
    }
}
