package pl.i5b4s1.ai.model;

import lombok.*;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "priceList")
public class PriceOfTheService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_price_of_the_service")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "id_medical_service")
    private MedicalService medicalService;
    @ManyToOne
    @JoinColumn(name = "id_doctor")
    private Doctor doctor;
    @Column(name = "price")
    private Double price;
}
