package pl.i5b4s1.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.i5b4s1.ai.model.MedicalService;

import java.util.Optional;

@Repository
public interface MedicalServiceRepository extends JpaRepository<MedicalService, Integer> {
    Optional<MedicalService> findByName(String name);
}
