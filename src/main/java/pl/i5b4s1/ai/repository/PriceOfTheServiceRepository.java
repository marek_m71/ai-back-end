package pl.i5b4s1.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.i5b4s1.ai.model.PriceOfTheService;

@Repository
public interface PriceOfTheServiceRepository extends JpaRepository<PriceOfTheService, Integer> {
}
