package pl.i5b4s1.ai.exception;

public class IncorrectOldPassword extends RuntimeException {
    public IncorrectOldPassword(String email) {
        super("Niepoprawne stare hasło dla użytkownika: " + email);

    }
}
